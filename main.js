const startScreen = document.querySelector(".start-screen");
const GetPlayerName = document.getElementsByTagName("input")[0];
const scoreContainer = document.querySelector(".score-container");
const playerName = document.querySelector(".score :nth-child(2)");
const playerScore = document.querySelector(".player-score :nth-child(1)");
const computerScore = document.querySelector(".computer-score :nth-child(1)");
const choicesScreen = document.querySelector(".choices-screen");
const rock = document.querySelector(".rock");
const paper = document.querySelector(".paper");
const scissors = document.querySelector(".scissors");
const shakingScreen = document.querySelector(".shaking-screen");
const resultScreen = document.querySelector(".result-screen");
const gameOverScreen = document.querySelector(".game-over-screen");

let choices = ["rock", "paper", "scissors"];
let playerChoices = [rock, paper, scissors];

let playerPoints = 0;
let computerPoints = 0;

function getIcon(choice) {
	let icon;
	if (choice === 0) {
		icon = "fa-hand-back-fist";
	} else if (choice === 1) {
		icon = "fa-hand";
	} else if (choice === 2) {
		icon = "fa-hand-scissors";
	}
	return icon;
}

function gameOver() {
	if (playerPoints === 3) {
		resultScreen.style.display = "none";
		gameOverScreen.style.display = "block";
		gameOverScreen.innerHTML = `<h2>Game Over</h2><p>Congratulations!<br>You win the game!</p><button onclick="window.location.reload()">Play again</button>`;
	} else if (computerPoints === 3) {
		resultScreen.style.display = "none";
		gameOverScreen.style.display = "block";
		gameOverScreen.innerHTML = `<h2>Game Over</h2><p>You Lose!<br> Try again next time!</p><button onclick="window.location.reload()">Play again</button>`;
	}
}

function round(e) {
	choicesScreen.style.display = "none";
	shakingScreen.style.display = "flex";
	setTimeout(() => {
		shakingScreen.style.display = "none";
		resultScreen.style.display = "block";
		playerScore.innerText = playerPoints;
		computerScore.innerText = computerPoints;
		gameOver();
	}, 2000);

	const playerChoice = choices.indexOf(e.currentTarget.className);
	const computerChoice = Math.floor(Math.random() * 3);
	let playerHand = `<i class="player-hand fa-regular ${getIcon(playerChoice)}"></i>`;
	let computerHand = `<i class="computer-hand fa-regular ${getIcon(computerChoice)}"></i>`;
	let nextRoundButton = `<button onclick="game()">Next round</button>`;

	if (playerChoice === computerChoice) {
		resultScreen.innerHTML =
			`
                 <div class="round-result">
                        ${playerHand}             
                        <h3>It's a tie</h3>
                        ${computerHand}
                 </div>  
            ` + nextRoundButton;
	} else if (playerChoice - computerChoice === 1 || playerChoice - computerChoice === -2) {
		playerPoints++;
		resultScreen.innerHTML =
			`
                <div class="round-result">
			        ${playerHand}
                    <div>
                        <h3>You Win!</h3>
                        <p>${choices[playerChoice]} beats ${choices[computerChoice]}</p>
                    </div>
                    ${computerHand}
                </div>
            ` + nextRoundButton;
	} else {
		computerPoints++;
		resultScreen.innerHTML =
			`
                <div class="round-result">
                    ${playerHand}
                    <div>
                        <h3>You Lose!</h3>
                        <p>${choices[playerChoice]} loses to ${choices[computerChoice]}</p>
                    </div>
                    ${computerHand}
                </div>
            ` + nextRoundButton;
	}
}

function game() {
	if (!GetPlayerName.value) {
		alert("Please enter your name!");
		return;
	}

	startScreen.style.display = "none";
	resultScreen.style.display = "none";
	choicesScreen.style.display = "flex";
	scoreContainer.style.display = "flex";
	playerName.innerHTML = GetPlayerName.value;

	playerChoices.forEach((choice) => {
		choice.addEventListener("click", round);
	});
}
